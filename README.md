# FireOver



## Getting started

Vous allez faire ceci comme commande.

### etape 1 :
    
    Assurez vous que vous avez votre clé ssh pour le gitlab :
    Suivre ce lien pour generer votre clé ssh et la rajouter a gitlab.
    https://www.w3schools.com/git/git_security_ssh.asp?remote=gitlab

    ** Dans le cas ou cela ne marches pas et vous avez des erreurs :
        *** Etape 1 : Allez vers votre dossier /.ssh
            Sous windows C:\Users\username\.ssh
            Sous Linux   /home/username/.ssh

        *** Etape 2 : Supprimer les clés qui se trouvent à l'interieur
        *** Refaire tout a partir du lien que je vous ai donné en haut

### etape 2 

    Veillez aller vers issues -> board 
    Puis selectionner une issue dans le board Ready
    Assigné la a votre nom dans la bare de droite.
    A l'aide de la souris glisser cette issue vers le board Doing.

### etape 3 

    Avec le terminal rendez vous dans votre projet.

    *** 1 - creation d'une nouvelle branche avec 'git checkout -b "le_nom_de_votre_issue"
    *** 2 - commancez a coder et faire des tests apres quabd tout est bon ->
    *** 3 - ajouter vos fichier dans votre branch local avec git add .
    *** 4 - commiter vos fichiers avec git commit -m "une legere bref discription"
    *** 5 - pusher votre tache avec git push
    *** 5 Bis - remarque : des fois il va vous dire de refaire avec le push avec une instrution que git va vous suggerer avec up-stream, vous copier coller la commande et vous l'executer.

    *** 6 - glissez votre issue dans need_review pour je puisse regarder le travail
    *** 7 - veillez m'envoyer une message pour que je puisse valider la tache.
    *** 8 - une fois valider veillez prendre une nouvelle tache et faire une nouvelle branche apres 'git pull' 
                    *** Et rebolot :)  *** 

### Remarque importante : ne pas toucher au code des autres. 

