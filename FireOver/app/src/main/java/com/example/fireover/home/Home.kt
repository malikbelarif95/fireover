package com.example.fireover.home

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MarkerInfoWindow
import com.google.maps.android.compose.MarkerState


@Composable
fun Home() {
    //cette liste de marquers c est juste pour le test ca doit etre injectés par la bdd avec les localisation des devices
    // j'utilise une data class MarkerData vous pouvez l utiliiser ou bien l enlevez comme vous voulez
    val markers = listOf(

        MarkerData(latitude = 49.864716, longitude = 3.349014, info = "Marker 1"),
        MarkerData(latitude = 48.864716, longitude = 2.349014, info = "Marker 2"),
        MarkerData(latitude = 48.864716, longitude = 2.949014, info = "Marker 3")
    )
    MarqueLocation(markers)
}


//la fonction appbar est pour ajouter un bar au dessus de l app, qui n est pas utilisée ici
@Composable
fun AppBar(
    onNavigationIconClick: () -> Unit,
    onBackPressed: () -> Unit
) {
    TopAppBar(
        title = { Text("Map") },
        navigationIcon = {
            Icon(
                imageVector = Icons.Default.Menu,
                contentDescription = "Home",
                modifier = Modifier.clickable(onClick = onNavigationIconClick)
            )
        },
        backgroundColor = MaterialTheme.colors.primary,
        contentColor = MaterialTheme.colors.onPrimary,
        actions = {
            IconButton(onClick = onBackPressed) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Back"
                )
            }
        }
    )
}

@Composable
fun marker(latitude : Double , longitude : Double, info:String) {
    val singapore = LatLng(latitude, longitude)
    MarkerInfoWindow(
        state = MarkerState(position = singapore),
    ) {
        Box(
            modifier = Modifier
                .background(
                    color = Color.Green,
                    shape = RoundedCornerShape(35.dp, 35.dp, 35.dp, 35.dp)
                )
                .width(500.dp)
            , contentAlignment = Alignment.BottomEnd

        ) {


            Column(
                modifier = Modifier.padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                //.........................Spacer
                Spacer(modifier = Modifier.height(24.dp))
                //.........................Text: title
                Text(
                    text = "Marker Title",
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp)
                        .fillMaxWidth(),
                )
                Spacer(modifier = Modifier.height(8.dp))
                //.........................Text : description
                Text(
                    text = info,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp, start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                )
                //.........................Spacer
                Spacer(modifier = Modifier.height(24.dp))

            }

        }

    }
}
@Composable
fun MarqueLocation(markers: List<MarkerData>) {
    GoogleMap(
        modifier = Modifier
            .fillMaxSize()
    ) {
        for (mark in markers){
            marker(latitude = mark.latitude, longitude =mark.longitude , info = mark.info)
        }
    }
}

@Composable
fun GoogleMapWithMarkers() {
    val context = LocalContext.current
    val mapView = MapView(context).apply {
        onCreate(null)
        getMapAsync { googleMap ->
            // Add three markers to the map
            val marker1 = LatLng(37.4220, -122.0841)
            val marker2 = LatLng(37.7749, -122.4194)
            val marker3 = LatLng(38.5816, -121.4944)
            googleMap.addMarker(MarkerOptions().position(marker1))
            googleMap.addMarker(MarkerOptions().position(marker2))
            googleMap.addMarker(MarkerOptions().position(marker3))

            // Wait for the first layout pass of the MapView
            viewTreeObserver.addOnGlobalLayoutListener {
                // Move the camera to show all three markers
                val bounds = LatLngBounds.builder()
                    .include(marker1)
                    .include(marker2)
                    .include(marker3)
                    .build()
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

                // Enable zoom controls and other UI features
                googleMap.uiSettings.isZoomControlsEnabled = true
                googleMap.uiSettings.isCompassEnabled = true
                googleMap.uiSettings.isMyLocationButtonEnabled = true
            }
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        AndroidView(
            modifier = Modifier.fillMaxSize(),
            factory = { mapView }
        )
    }
}


data class MarkerData(val latitude: Double, val longitude: Double, val info: String)
@Preview(showBackground = true)
@Composable
fun HomePreview(){
    Home()
}