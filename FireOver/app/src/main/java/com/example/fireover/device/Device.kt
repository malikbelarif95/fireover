package com.example.fireover.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.example.fireover.R
import com.example.fireover.bean.Dev
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


@SuppressLint("SuspiciousIndentation")
@Composable
fun MyScreen(
) {
    var isError = false
    Surface(color = MaterialTheme.colors.background) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            var name  by remember { mutableStateOf(TextFieldValue()) }
            var idApi by remember { mutableStateOf(TextFieldValue()) }


            OutlinedTextField(
                value = name,
                onValueChange = { name = it },
                label = { Text("Name") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                isError = isError,
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Filled.AddCircle,
                        contentDescription = null,
                    )
                }
            )
            Spacer(modifier = Modifier.height(16.dp))

            OutlinedTextField(
                value = idApi,
                onValueChange = { idApi = it},
                label = { Text("API ID") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                isError = isError,
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Filled.CheckCircle,
                        contentDescription = null,
                    )
                }
            )

            Spacer(modifier = Modifier.height(16.dp))



            //LocationEditText()
            val context = LocalContext.current
            var location by remember { mutableStateOf<Location?>(null) }
            var latLngString by remember { mutableStateOf("") }

            val permissionLauncher = rememberLauncherForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted ->
                if (isGranted) {
                    // Permission granted, get current location
                    val locationManager =
                        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        0L,
                        0f,
                        object : LocationListener {
                            override fun onLocationChanged(l: Location) {
                                location = l
                                latLngString ="${l.latitude}, ${l.longitude}"
                            }

                            @Deprecated("Deprecated in Java")
                            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                            override fun onProviderEnabled(provider: String) {}

                            override fun onProviderDisabled(provider: String) {}
                        }
                    )
                } else {
                    // Permission not granted, show error or default value
                    latLngString = "Location not available"
                }
            }

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                OutlinedTextField(
                    modifier = Modifier
                        .padding(16.dp),
                    value = latLngString,
                    onValueChange = { latLngString = it },
                    label = { Text("Location") },
                    readOnly = true,
                    isError = isError,
                    leadingIcon = {
                        val imageVector = ImageVector.vectorResource(id = R.drawable.ic_baseline_my_location_24)
                        Icon(
                            imageVector = imageVector,
                            contentDescription = null
                        )
                    },

                    )

                IconButton(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .width(60.dp),
                    onClick = {
                        if (ContextCompat.checkSelfPermission(
                                context,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            // Permission already granted, get current location
                            val locationManager =
                                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                            val ll = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                            if (ll != null) {
                                location = ll
                                latLngString = "${ll.latitude}, ${ll.longitude}"
                            } else {
                                latLngString ="Location not available"
                            }
                        } else {
                            // Permission not granted, request it
                            permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                        }
                    }
                ){
                    Icon(
                        imageVector = Icons.Rounded.LocationOn,
                        contentDescription = null,
                        tint = MaterialTheme.colors.primary
                    )
                }
            }
            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    val nameText = name.text
                    val idApiText = idApi.text
                    val locationText = location?.longitude.toString() + "," + location?.latitude.toString()

                        Toast.makeText(context, "Submit", Toast.LENGTH_SHORT).show()
                        Toast.makeText(context, nameText, Toast.LENGTH_SHORT).show()
                        Toast.makeText(context, idApiText, Toast.LENGTH_SHORT).show()
                        Toast.makeText(context, locationText, Toast.LENGTH_SHORT).show()

                    if (nameText.isEmpty() || idApiText.isEmpty() || locationText.isEmpty()) {
                        isError = true
                    }

                    checkIdApiOrNameDevice(nameText, idApiText) { isExist ->
                        // Do something with the isExist value here
                        if (isExist) {
                            Toast.makeText(context, "The device already exists", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(context, "The device does not exist", Toast.LENGTH_SHORT).show()
                            addDevice(name.text, idApi.text, GeoPoint(location?.latitude!!, location?.longitude!!))
                        }
                    }
                }
            ) {
                Text("Submit")
            }
        }
    }
}

data class devices(
    val name: String,
    val idApi: String,
    val location: GeoPoint,
    val temperature : Double,
    val humidity : Double,
    val co2 : Double
)