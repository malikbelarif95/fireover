package com.example.fireover

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.rounded.ExitToApp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.fireover.Showdata.TemperatureDisplay

import com.example.fireover.device.MyScreen
import com.example.fireover.home.GoogleMapWithMarkers
import com.example.fireover.list.ListeDeviceScreen
import com.example.fireover.ui.theme.FireOverTheme

class SwitchedBarActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FireOverTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MyAppContent()
                }
            }
        }
    }
}

@Composable
fun MyAppContent() {
    val navController = rememberNavController()
    val items = listOf(
        Screen.Home,
        Screen.AddDevice,
        Screen.Map,
        Screen.datas,
    )
    Scaffold(
        bottomBar = {
            BottomNavigation {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentRoute = navBackStackEntry?.destination?.route
                items.forEach { screen ->
                    BottomNavigationItem(
                        icon = { Icon(screen.icon, contentDescription = null) },
                        label = { Text(screen.title) },
                        selected = currentRoute == screen.route,
                        onClick = {
                            if (screen.route == "logout") {

                            }
                            navController.navigate(screen.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.startDestinationId)
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                            }
                        }
                    )
                }

            }
        }
    ) {
        NavHost(
            navController = navController,
            startDestination = Screen.Home.route
        ) {
            composable(Screen.Home.route) { GoogleMapWithMarkers() }
            composable(Screen.AddDevice.route) { MyScreen() }
            composable(Screen.Map.route) { ListeDeviceScreen() }
            composable (Screen.datas.route){TemperatureDisplay(
                c = 2f, h = 5f, t = 10f
            )}
        }
    }
}



sealed class Screen(val route: String, val title: String, val icon: ImageVector) {
    object Home : Screen("home", "Home", Icons.Default.Home)
    object AddDevice : Screen("Add Device", "Add device", Icons.Default.Add)
    object Map : Screen("Liste", "liste", Icons.Default.LocationOn)
    object datas : Screen("details","details",Icons.Default.Done)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview4() {
    FireOverTheme {
    }
}
