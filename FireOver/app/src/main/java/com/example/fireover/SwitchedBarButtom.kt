package com.example.fireover

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ExitToApp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.fireover.device.MyScreen
import com.example.fireover.home.GoogleMapWithMarkers
//import com.example.fireover.home.Home
import com.example.fireover.list.ListeDeviceScreen
import com.example.fireover.repo.AuthRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


@Composable
fun SwitchedButtomBar() {
    val context = LocalContext.current
    val navController = rememberNavController()
    val intent = Intent(context,MainActivity::class.java)
    val items = listOf(
        Screen.Home,
        Screen.AddDevice,
        Screen.Map,
    )
    Scaffold(
        bottomBar = {
            BottomNavigation {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentRoute = navBackStackEntry?.destination?.route
                items.forEach { screen ->
                    BottomNavigationItem(
                        icon = { Icon(screen.icon, contentDescription = null) },
                        label = { Text(screen.title) },
                        selected = currentRoute == screen.route,
                        onClick = {
                            if (screen.route == "logout") {

                            }
                            navController.navigate(screen.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.startDestinationId)
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                            }
                        }
                    )
                }
                BottomNavigationItem(
                    icon = { Icon(Icons.Rounded.ExitToApp, contentDescription = null) },
                    label = { Text("Logout") },
                    selected = false,
                    onClick = {
                        val db = Firebase.firestore
                        val auth = FirebaseAuth.getInstance()
                        Log.d("Logout", "Logout button clicked")
                        auth.signOut()
                        // Or if you're using SharedPreferences to clear user data:
                        val sharedPrefs = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE)
                        sharedPrefs.edit().clear().apply()
                        Log.d("Logout", "Logout button clicked")
                        context.startActivity(intent)
                    }
                )
            }
        }
    ) {
        NavHost(
            navController = navController,
            startDestination = Screen.Home.route
        ) {
            composable(Screen.Home.route) { GoogleMapWithMarkers() }
            composable(Screen.AddDevice.route) { MyScreen() }
            composable(Screen.Map.route) { ListeDeviceScreen() }
        }
    }
}

fun logOut(firebaseAuth: FirebaseAuth) {
    firebaseAuth.signOut()
}


