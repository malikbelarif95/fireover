package com.example.fireover.Showdata

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.fireover.Showdata.ui.theme.FireOverTheme
import com.example.fireover.SwitchedBarActivity

class DataDisplayActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FireOverTheme {
                val context = LocalContext.current
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        val temperature = intent.getStringExtra("temperature")?.toFloat()
                        val humidity = intent.getStringExtra("humidity")?.toFloat()
                        val co2 = intent.getStringExtra("co2")?.toFloat()

                        Toast.makeText(context, temperature.toString(), Toast.LENGTH_SHORT).show()
                            Row{
                                if (temperature != null) {
                                    if (humidity != null) {
                                        if (co2 != null) {
                                            TemperatureDisplay(t = temperature, h = humidity, c = co2)
                                        }
                                    }
                                }
                            }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting3(name: String) {
    Text(text = "Hello $name!")
}

@Composable
fun BackButton() {
    val context = LocalContext.current
    val backDispatcher = LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher

    val backCallback = remember {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                context.startActivity(Intent(context, SwitchedBarActivity::class.java))
            }
        }
    }

    DisposableEffect(Unit) {
        backCallback.isEnabled = true
        if (backDispatcher != null) {
            backDispatcher.addCallback(backCallback)
        }
        onDispose {
            backCallback.remove()
        }
    }

    TextButton(
        onClick = {
            context.startActivity(Intent(context, SwitchedBarActivity::class.java))
        }
    ) {
        Text("Back", fontSize = 24.sp)
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview3() {
    FireOverTheme {
        BackButton()
        TemperatureDisplay(t = 20.0f, h = 30.0f, c = 4000.0f)
    }
}