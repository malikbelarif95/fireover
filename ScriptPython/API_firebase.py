import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import time
import requests
from flask import Flask, jsonify
from flask import stream_with_context, request
import json
from multiprocessing import Process, Value

 # Initialisez une application Firebase avec vos informations d'identification
cred = credentials.Certificate("./fireover.json")
firebase_admin.initialize_app(cred)

def getObject_octaveID(classs):
    # Initialisez Firestore
    db = firestore.client()

    # Récupérez une référence à la collection "Ids"
    ids_ref = db.collection(classs)

    # Récupérez tous les documents dans la collection "Ids"
    ids_docs = ids_ref.get()

    # Initialisez une liste pour stocker les objets récupérés depuis Firestore
    objects = []

    # Parcourez tous les documents et ajoutez leur contenu à la liste des objets
    for doc in ids_docs:
        objects.append(doc.to_dict()["OctaveId"])

    # Retournez la liste des objets récupérés depuis Firestore
    return objects

def getObject_ID(classs):
    # Initialisez Firestore
    db = firestore.client()

    # Récupérez une référence à la collection "Ids"
    ids_ref = db.collection(classs)

    # Récupérez tous les documents dans la collection "Ids"
    ids_docs = ids_ref.get()

    # Initialisez une liste pour stocker les objets récupérés depuis Firestore
    objects = []

    # Parcourez tous les documents et ajoutez leur contenu à la liste des objets
    for doc in ids_docs:
        objects.append(doc.id)

    # Retournez la liste des objets récupérés depuis Firestore
    return objects

def updateObject(Id_API,t,h,c):
    # Initialisez Firestore
    db = firestore.client()

    # Récupérez une référence à la collection "users"
    users_ref = db.collection("devices")

    # Définissez l'ID du document que vous voulez mettre à jour
    doc_id = Id_API

    # Mettez à jour le document avec les nouvelles données
    users_ref.document(doc_id).update({
        "humidity": h,
        "co2": c,
        "temperature": t,
    })

    # Vérifiez que les données ont été mises à jour en les récupérant à nouveau depuis Firestore
    doc = users_ref.document(doc_id).get()
    print(doc.id)
    print(doc.to_dict())

app = Flask(__name__)

@app.route('/')
def streamed_response():
    def generate():
        i = 0
        wrapper = getObject_octaveID("Ids")
        while True:
            headers = {
                "X-Auth-Token": "RpkBuNbT4gEG5AjuqupLzpPlSuj9sTUr",
                "X-Auth-User": "fireover"
            }
            responses = requests.get(
                f"https://octave-api.sierrawireless.io/v5.0/FireOver/device/{wrapper[i]}/",
                headers=headers)
            res = json.dumps(responses.json(), indent=4)
            res = json.loads(res)
            res = res["body"]["summary"]
            res = json.dumps(res["/environment/value"]["v"])[1:-1]
            res = res.replace("\\", "")
            temp = str(json.loads(res)['temperature'])
            hum = str(json.loads(res)['humidity'])
            co2 = str(json.loads(res)['co2EquivalentValue'])
            print("temperature : " + temp)
            print("humidity : " + hum)
            print("Co2² : " + co2)

            new_id = getObject_ID("devices")

            print("new_id"+new_id[i])
            updateObject(new_id[i],temp,hum,co2)
            html = "<h4> temperature : " + temp + " humidity : " + hum + " Co2² : " + co2 + ": " + str(i) + "</h4>"
            yield f"{html}"
            i += 1
            i = 0 if i >= len(new_id) else i
            time.sleep(10)

    return app.response_class(stream_with_context(generate()))

if __name__ == '__main__':
    port = 8000  # the custom port you want
    app.run(host='0.0.0.0', port=port)