package com.example.fireover.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.snapshots.SnapshotStateMap
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.fireover.SwitchedBarActivity
import com.example.fireover.home.ui.theme.FireOverTheme
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MarkerInfoWindow
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import kotlinx.coroutines.launch

class MapActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FireOverTheme {
                // A surface container using the 'background' color from the theme
                val scaffoldState = rememberScaffoldState()
                val scop = rememberCoroutineScope()
                Scaffold(
                    scaffoldState = scaffoldState,
                    topBar = {
                        AppBar(
                            onNavigationIconClick = {
                                scop.launch {
                                    scaffoldState.drawerState.open()
                                }
                            },
                            onBackPressed = {
                                onBackPressedDispatcher.onBackPressed()
                            }
                        )
                    }

                ){
                    val Firestore= FirestoreData()

                    MarqueLocation(Firestore)
                }
            }
        }
    }
}

@Composable
fun Greeting2(name: String) {
    Text(text = "Hello $name!")
}



@Composable
fun AppBar(
    onNavigationIconClick: () -> Unit,
    onBackPressed: () -> Unit
) {
    val context = LocalContext.current
    val intent = Intent(context, SwitchedBarActivity::class.java)
    TopAppBar(
        title = { Text("Map") },
        navigationIcon = {
            Icon(
                imageVector = Icons.Default.Menu,
                contentDescription = "Home",
                modifier = Modifier.clickable(onClick = onNavigationIconClick)
            )
        },
        backgroundColor = MaterialTheme.colors.primary,
        contentColor = MaterialTheme.colors.onPrimary,
        actions = {
            IconButton(onClick = {
                    context.startActivity(intent)
            }) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Back"
                )
            }
        }
    )
}






@Composable
fun marker(latitude : Double , longitude : Double, name :String, temperature: Double, humidity: Double, co2: Any) {
    val singapore = LatLng(latitude, longitude)
    MarkerInfoWindow(
        state = MarkerState(position = singapore),
    ) {
        Box(
            modifier = Modifier
                .background(
                    color = Color.Green,
                    shape = RoundedCornerShape(35.dp, 35.dp, 35.dp, 35.dp)
                )
                .width(500.dp)
            , contentAlignment = Alignment.BottomEnd

        ) {


            Column(
                modifier = Modifier.padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                //.........................Spacer
                Spacer(modifier = Modifier.height(24.dp))
                //.........................Text: title
                Text(
                    text = name,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp)
                        .fillMaxWidth(),
                )
                Spacer(modifier = Modifier.height(4.dp))
                //.........................Text : description
                Text(
                    text = "Temperature: "+temperature.toString(),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp, start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                )
                Text(
                    text = "Humidity: "+humidity.toString(),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp, start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                )
                Text(
                    text = "Co2: "+co2.toString(),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(top = 10.dp, start = 25.dp, end = 25.dp)
                        .fillMaxWidth(),
                )

                //.........................Spacer
                Spacer(modifier = Modifier.height(24.dp))

            }

        }

    }
}

@Composable
fun MarqueLocation(Firestore: SnapshotStateList<Map<String, Any>>) {

    GoogleMap(
        cameraPositionState = rememberCameraPositionState {
            position = CameraPosition.fromLatLngZoom(LatLng(48.8534322, 2.5197396),14f)
        },
        modifier = Modifier
            .fillMaxSize()
    ){

        Firestore.forEach { data ->

            val latitude = (data["location"] as GeoPoint).latitude
            val longitude = (data["location"] as GeoPoint).longitude
            val name = data["name"] as String
            val temp=data["temperature"] as Double
            val humidity=data["humidity"] as Double
            val co2=data["co2"] as Any

            //Text("$latitude, $longitude, $info")

            marker(latitude = latitude, longitude = longitude, name = name, temperature = temp, humidity = humidity, co2 = co2)
        }



    }
}


@Composable
fun FirestoreData(): SnapshotStateList<Map<String, Any>> {
    val db = Firebase.firestore
    val collectionRef = db.collection("devices")
    val dataList = remember { mutableStateListOf<Map<String, Any>>() }

    LaunchedEffect(Unit) {
        collectionRef.get().addOnSuccessListener { querySnapshot ->
            for (document in querySnapshot.documents) {
                val data = document.data
                if (data != null) {
                    dataList.add(data)
                }
            }
        }.addOnFailureListener { exception ->
            Log.e("Firestore", "Error getting documents: ", exception)
        }
    }

    return dataList
}



data class MarkerData(val latitude: Double, val longitude: Double, val info: String)

