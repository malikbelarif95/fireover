package com.example.fireover.home


import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.fireover.MyAppContent


import com.example.fireover.login.LoginScreen
import com.example.fireover.login.LoginViewModel
import com.example.fireover.login.SignUpScreen


enum class LoginRoutes {
    Signup,
    SignIn
}

enum class HomeRoutes {
    Home,
    Detail
}

enum class SwitchedButtomBarRoutes {
    SwtichedButtomBar
}
@Composable
fun Navigation(
    navController: NavHostController = rememberNavController(),
    loginViewModel: LoginViewModel,
) {
    NavHost(
        navController = navController,
        startDestination = LoginRoutes.SignIn.name
    ) {
        composable(route = LoginRoutes.SignIn.name) {
            LoginScreen(onNavToSwitcheButtomBarPage = {
                navController.navigate(SwitchedButtomBarRoutes.SwtichedButtomBar.name) {
                    launchSingleTop = true
                    popUpTo(route = LoginRoutes.SignIn.name) {
                        inclusive = true
                    }
                }
            },
                loginViewModel = loginViewModel

            ) {
                navController.navigate(LoginRoutes.Signup.name) {
                    launchSingleTop = true
                    popUpTo(LoginRoutes.SignIn.name) {
                        inclusive = true
                    }
                }
            }
        }

        composable(route = LoginRoutes.Signup.name) {
            SignUpScreen(onNavToSwitcheButtomBarPage = {
                navController.navigate(SwitchedButtomBarRoutes.SwtichedButtomBar.name) {
                    popUpTo(LoginRoutes.Signup.name) {
                        inclusive = true
                    }
                }
            },
                loginViewModel = loginViewModel
            ) {
                navController.navigate(LoginRoutes.SignIn.name)
            }
        }
        composable(route = SwitchedButtomBarRoutes.SwtichedButtomBar.name) {
            MyAppContent()
        }
    }


}