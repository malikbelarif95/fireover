package com.example.fireover.Showdata

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TemperatureDisplay(t: Float, h: Float, c: Float) {
    Box(Modifier.fillMaxSize()) {
        val temperature1Progress = remember { mutableStateOf(t / 100f) }
        val temperature2Progress = remember { mutableStateOf(h / 100f) }
        val temperature3Progress = remember { mutableStateOf(c / 5000f) }
        Column(
            Modifier.align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TemperatureBar(
                progress = temperature1Progress.value,
                temperature = String.format("%.2f", t),
                color = Color.Blue,
                title = "Temperature",
                unit = "°"
            )
            Spacer(Modifier.height(16.dp))
            TemperatureBar(
                progress = temperature2Progress.value,
                temperature = String.format("%.2f", h),
                color = Color.Red,
                title = "Humidity",
                unit = "%"
            )
            Spacer(Modifier.height(16.dp))
            TemperatureBar(
                progress = temperature3Progress.value,
                temperature = String.format("%.2f", c),
                color = Color.Green,
                title = "CO2",
                unit = "ppm"
            )
        }
        TemperatureProgressBar(progress = temperature1Progress, color = Color.Blue)
        TemperatureProgressBar(progress = temperature2Progress, color = Color.Red)
        TemperatureProgressBar(progress = temperature3Progress, color = Color.Green)
    }
}

@Composable
fun TemperatureBar(progress: Float, temperature: String, color: Color, title: String, unit: String) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        contentAlignment = Alignment.Center
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = title, fontSize = 20.sp)
            Spacer(Modifier.height(8.dp))
            Box(
                modifier = Modifier
                    .size(200.dp)
                    .background(Color.LightGray, shape = CircleShape)
            ) {
                TemperatureCircle(
                    progress = progress,
                    temperature = temperature,
                    color = color,
                    unit = unit
                )
            }
        }
    }
}
@Composable
fun TemperatureCircle(progress: Float, temperature: String, color: Color, unit: String) {
    val stroke = with(LocalDensity.current) { Stroke(16.dp.toPx()) }
    Canvas(modifier = Modifier.size(200.dp)) {
        val center = Offset(size.width / 2, size.height / 2)
        val radius = size.minDimension / 2 - stroke.width / 2
        val angle = progress * 360
        drawArc(
            color = color,
            startAngle = -90f,
            sweepAngle = angle,
            useCenter = false,
            topLeft = Offset(center.x - radius, center.y - radius),
            size = Size(radius * 2, radius * 2),
            style = stroke
        )
        drawIntoCanvas { canvas ->
            val paint = Paint().asFrameworkPaint()
            paint.textSize = 48.dp.toPx()
            paint.textAlign = android.graphics.Paint.Align.CENTER

            if (unit == "ppm") {
                canvas.nativeCanvas.drawText("${temperature}",
                    center.x,
                    center.y + paint.textSize / 3,
                    paint
                )
                paint.textSize = 30.dp.toPx()
                canvas.nativeCanvas.drawText("${unit}",
                    center.x,
                    center.y + paint.textSize * 2,
                    paint
                )
            }
            else {
                canvas.nativeCanvas.drawText(
                    "${temperature}${unit}",
                    center.x,
                    center.y + paint.textSize / 3,
                    paint
                )
            }
        }
    }
}

@Composable
fun TemperatureProgressBar(progress: MutableState<Float>, color: Color) {
    LinearProgressIndicator(
        progress = progress.value,
        modifier = Modifier.fillMaxWidth(),
        color = color,
        backgroundColor = Color.LightGray
    )
}