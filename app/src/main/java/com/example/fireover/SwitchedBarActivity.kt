package com.example.fireover

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.rounded.ExitToApp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.fireover.device.MyScreen

import com.example.fireover.list.ListeDeviceScreen
import com.example.fireover.ui.theme.FireOverTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class SwitchedBarActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FireOverTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MyAppContent()
                }
            }
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MyAppContent() {
    val navController = rememberNavController()
    val context = LocalContext.current
    val intent = Intent(context,MainActivity::class.java)
    val items = listOf(
        Screen.Home,
        Screen.AddDevice,
        Screen.Map
    )
    Scaffold(
        bottomBar = {
            BottomNavigation {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentRoute = navBackStackEntry?.destination?.route
                items.forEach { screen ->
                    BottomNavigationItem(
                        icon = { Icon(screen.icon, contentDescription = null) },
                        label = { Text(screen.title) },
                        selected = currentRoute == screen.route,
                        onClick = {
                            navController.navigate(screen.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.startDestinationId)
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                            }
                        }
                    )

                }
                BottomNavigationItem(
                    icon = { Icon(Icons.Rounded.ExitToApp, contentDescription = null) },
                    label = { Text("Logout") },
                    selected = false,
                    onClick = {
                        val db = Firebase.firestore
                        val auth = FirebaseAuth.getInstance()
                        Log.d("Logout", "Logout button clicked")
                        val sharedPrefs = context.getSharedPreferences("session", Context.MODE_PRIVATE)
                        sharedPrefs.edit().clear().apply()
                        auth.signOut()
                        Log.d("Logout", "Logout button clicked")
                        context.startActivity(intent)
                    }
                )

            }
        }
    ) {
        NavHost(
            navController = navController,
            startDestination = Screen.AddDevice.route
        ) {

            composable(Screen.Home.route) { context.startActivity(Intent(context,com.example.fireover.home.MapActivity::class.java)) }
            composable(Screen.AddDevice.route) { MyScreen() }
            composable(Screen.Map.route) { ListeDeviceScreen() }

        }
    }
}



sealed class Screen(val route: String, val title: String, val icon: ImageVector) {
    object Home : Screen("home", "Home", Icons.Default.Home)
    object AddDevice : Screen("Add Device", "Add device", Icons.Default.Add)
    object Map : Screen("Liste", "liste", Icons.Default.LocationOn)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview4() {
    FireOverTheme {
    }
}
@Composable
fun MyContent(){

    // Fetching the Local Context
    val mContext = LocalContext.current

    Column(Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {

        // Creating a Button that on-click
        // implements an Intent to go to SecondActivity
        Button(onClick = {
            mContext.startActivity(Intent(mContext,com.example.fireover.home.MapActivity::class.java))
        },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0XFF0F9D58)),
        ) {
            Text("Go to Second Activity", color = Color.White)
        }
    }
}