package com.example.fireover.device

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.fireover.bean.Ids
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


// function to inert data to firebase firestore database
fun addDevice(name: String, idApi: String, location: GeoPoint) {

    val db = Firebase.firestore
    val device = devices(name, idApi, location, 0.0, 0.0, 0.0)
    val ids= Ids(idApi)
    // ajout de l'objet device
    db.collection("devices")
        .add(device)
        .addOnSuccessListener { documentReference ->
            Log.d("TAG", "DocumentSnapshot added with ID: ${documentReference.id}")
        }
        .addOnFailureListener { e ->
            Log.w("TAG", "Error adding document", e)
        }

    // ajouter id Api dans la collection ids
    db.collection("Ids")
        .add(ids)
        .addOnSuccessListener { documentReference ->
            Log.d("TAG", "DocumentSnapshot added with ID: ${documentReference.id}")
        }
        .addOnFailureListener { e ->
            Log.w("TAG", "Error adding document", e)
        }
}
// function to check if the device is already exist
fun checkIdApiOrNameDevice(name: String, idApi: String, callback: (Boolean) -> Unit) {
    val db = Firebase.firestore
    db.collection("devices")
        .whereEqualTo("name", name)
        .whereEqualTo("idApi", idApi)
        .get()
        .addOnSuccessListener { documents ->
            callback(!documents.isEmpty)
        }
        .addOnFailureListener { exception ->
            Log.w("TAG", "Error getting documents: ", exception)
            callback(false)
        }
}
