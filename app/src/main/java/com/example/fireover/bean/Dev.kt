package com.example.fireover.bean

import com.google.firebase.firestore.GeoPoint


data class Dev(
    val name: String,
    val location: GeoPoint,
    val idApi: String,
    val humidity: Double,
    val temperature: Double,
    val co2: Double
) {
    constructor() : this("", GeoPoint(0.0, 0.0), "", 0.0, 0.0, 0.0)
}