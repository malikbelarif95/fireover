package com.example.fireover.list

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.fireover.bean.Dev
import com.example.fireover.list.ui.theme.FireOverTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items

import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign

import androidx.compose.ui.unit.dp
import com.example.fireover.Showdata.DataDisplayActivity
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshState

import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await

class ListDeviceActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FireOverTheme {
                // A surface container using the 'background' color from the theme
                ListeDeviceScreen()
            }
        }
    }
}

private val Property = listOf("Name", "Position", "Temperature", "Humidity")



@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ListeDeviceScreen() {
    var devices by remember { mutableStateOf(emptyList<Dev>()) }
    var sortBy by remember { mutableStateOf( Property.first()) }

    LaunchedEffect(Unit) {
        val firestore = FirebaseFirestore.getInstance()
        val devicesSnapshot = firestore.collection("devices").get().await()
        devices = devicesSnapshot.toObjects(Dev::class.java)
    }
 Scaffold(
     topBar = {
         TopAppBar(
             title = { Text(text = "Device List")},
             actions =
             {
                     FilterList(Property, sortBy) { selected ->
                         sortBy = selected }
             }
         )

     }

 ) {
     Column {
         SwipeRefresh(state = SwipeRefreshState(
             isRefreshing = false,
         ) , onRefresh = {  }) {
             DeviceList(devices, sortBy)
         }
     }
 }
}

@Composable
fun FilterList(options: List<String>, selectedOption: String, onOptionSelected: (String) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    IconButton(onClick = { expanded = true }) {
        Text(text = "Filter by $selectedOption")
        DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            options.forEach { option ->
                DropdownMenuItem(onClick = {
                    onOptionSelected(option)
                    expanded = false
                }) {
                    Text(text = "Filter by $option",
                        textAlign = TextAlign.Right
                        )
                }
            }
        }
    }
}

@Composable
fun DeviceList(devices: List<Dev>, sortBy: String) {
    val sortedDevices = when (sortBy) {
        "Name" -> devices.sortedBy { it.name }
        "Temperature" -> devices.sortedBy { it.temperature }
        "Humidity" -> devices.sortedBy { it.humidity }
        else -> devices
    }
    LazyColumn(
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(sortedDevices) { device ->
            DeviceListItem(device)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DeviceListItem(device: Dev) {
    val context = LocalContext.current
    val intent = Intent(context, DataDisplayActivity::class.java)
    Card(
        shape = MaterialTheme.shapes.medium,
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        backgroundColor = MaterialTheme.colors.surface,
        onClick = {
            intent.putExtra("temperature",device.temperature.toString())
            intent.putExtra("humidity",device.humidity.toString())
            intent.putExtra("co2",device.co2.toString())
            Toast.makeText(context, device.temperature.toString(), Toast.LENGTH_SHORT).show()
            context.startActivity(intent)
        }
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = "Name : ${device.name}",
                style = MaterialTheme.typography.h6,
                color = MaterialTheme.colors.onSurface
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = "Position: ${device.location.latitude},${device.location.longitude}",                style = MaterialTheme.typography.subtitle2,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = "ID API: ${device.idApi}",
                style = MaterialTheme.typography.caption,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "Temperature: ${device.temperature}",
                style = MaterialTheme.typography.caption,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "Humidity: ${device.humidity}",
                style = MaterialTheme.typography.caption,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "Co2: ${device.co2}",
                style = MaterialTheme.typography.caption,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    FireOverTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            ListeDeviceScreen()
        }
    }
}